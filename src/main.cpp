#include <chrono>

#include <fmt/core.h>
#include <SDL2/SDL.h>
#include <sndfile.hh>

#include <resource.h>

uint32_t time_since(std::chrono::time_point<std::chrono::system_clock> start) {
    std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
    return std::chrono::duration_cast<std::chrono::microseconds>(now - start).count();
}

int main()
{
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        fmt::print(stderr, "Failed to init SDL\n");
        return -1;
    }


    // now decode and route for playback
    auto rm = Resource::ResourceManager();
    auto mp3file = rm.requestMP3("Yuyoyuppe_JustFollowTime");

    SF_FORMAT_INFO format_info;
    SF_INFO sf_info;
    int format, major_count, subtype_count, m, s;

    sf_command (NULL, SFC_GET_FORMAT_MAJOR_COUNT, &major_count, sizeof (int));
    sf_command (NULL, SFC_GET_FORMAT_SUBTYPE_COUNT, &subtype_count, sizeof (int));

    sf_info.channels = 2;

    SDL_Quit();
}
