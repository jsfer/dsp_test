#include <resource.h>

#include <cassert>

namespace Resource {

ResourceManager::ResourceManager() {
    auto base_path = std::filesystem::directory_entry("res");
    assert(base_path.exists() && "FATAL: res/ path doesnt exist!");
    assert(base_path.is_directory() && "FATAL: res/ path exists but its not a directory!");

    auto res_audio_path = std::filesystem::directory_entry("res/audio");
    assert(res_audio_path.exists() && "FATAL: res/ path doesnt exist!");
    assert(res_audio_path.is_directory() && "FATAL: res/ path exists but its not a directory!");

    auto res_audio_rendered_path = std::filesystem::directory_entry("res/audio/rendered");
    assert(res_audio_rendered_path.exists() && "FATAL: res/ path doesnt exist!");
    assert(res_audio_rendered_path.is_directory() && "FATAL: res/ path exists but its not a directory!");
}

ResourceManager::~ResourceManager() {
    loaded_assets.clear();
}

const std::optional<std::filesystem::directory_entry> ResourceManager::requestDI(const std::string& path) {
    auto requested = std::filesystem::directory_entry("res/audio/DI/" + path + ".wav");
    if (requested.exists()) {
        return { requested };
    } else {
        return { };
    }
}

const std::optional<std::filesystem::directory_entry> ResourceManager::requestMP3(const std::string& path) {
    auto requested = std::filesystem::directory_entry("res/audio/rendered/" + path + ".mp3");
    if (requested.exists()) {
        return { requested };
    } else {
        return { };
    }
}


}
