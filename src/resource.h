#pragma once

#include <filesystem>
#include <optional>
#include <string>
#include <vector>

/*
    Accessor functions for reading audio files from disk in designated directories.
    When a successful request to a resource is made,
    the ResourceManager will append its list, and provide references to the requested resource.
*/

namespace Resource {

class ResourceManager {
public:
    ResourceManager();
    ~ResourceManager();

    // request resource by filename, no extension (ie. for `res/DI/guitar_di.wav`: call requestDI("guitar_di"))
    const std::optional<std::filesystem::directory_entry> requestDI(const std::string& path);
    const std::optional<std::filesystem::directory_entry> requestMP3(const std::string& path);

private:
    std::vector<std::filesystem::directory_entry> loaded_assets;
};


}
