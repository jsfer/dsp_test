#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <filesystem>

#include <fmt/core.h>

#include <resource.h>

BOOST_AUTO_TEST_SUITE(dspResourceTestSuite)

BOOST_AUTO_TEST_CASE(ResourceManagerRequestTestCase)
{
    auto rm = Resource::ResourceManager();
    auto mp3 = std::string("Yuyoyuppe_JustFollowTime");
    auto req_opt = rm.requestMP3(mp3);
    BOOST_CHECK(req_opt.has_value());
}

BOOST_AUTO_TEST_SUITE_END()
